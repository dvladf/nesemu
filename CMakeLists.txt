cmake_minimum_required(VERSION 3.5)
project(NESemu)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(SOURCES src/main.cpp
			src/app.cpp
			src/controller.cpp
			src/cpu.cpp
			src/mapper.cpp
			src/ppu.cpp)

set(HEADERS src/app.h
			src/common.h
			src/controller.h
			src/cpu.h
			src/mapper.h
			src/ppu.h)

find_package(SDL2 REQUIRED)
find_package(OpenGL REQUIRED)

add_executable(NESemu ${SOURCES} ${HEADERS})
target_link_libraries(NESemu SDL2::SDL2 SDL2::SDL2main ${OPENGL_LIBRARY})
