# NESemu - Nintendo Entertainment System emulator

## Install dependencies:

### Debian/Ubuntu:
```
$ sudo apt install libsdl2-dev cmake
```

## Build

### Linux (g++):
```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
### Windows (MSVC)
1. Generate .sln for Visual Studio in cmake
2. Build .sln project in Visual Studio 

## Usage

### Windows
Just click on the executable file and choose rom file on the opened dialog.
### Linux 
Run in terminal:
```
./NESemu <path on the ROM file>
```

## Controls

Button A - Q

Button B - W

Button Select - S

Button Start - A

Arrows

Exit from emulator - Esc