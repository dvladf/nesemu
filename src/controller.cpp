#include "controller.h"

void Controller::SetButtons(bool *_buttons)
{
	buttons[0] = _buttons[0];
	buttons[1] = _buttons[1];
	buttons[2] = _buttons[2];
	buttons[3] = _buttons[3];
	buttons[4] = _buttons[4];
	buttons[5] = _buttons[5];
	buttons[6] = _buttons[6];
	buttons[7] = _buttons[7];
}

u8 Controller::Read()
{
	u8 val = 0;
	if (index < 8 && buttons[index])
	{
		val = 1;
	}
	index++;
	if ((strobe & 1) == 1)
	{
		index = 0;
	}
	return val;
}

void Controller::Write(u8 val)
{
	strobe = val;
	if (strobe == 1)
	{
		index = 0;
	}
}