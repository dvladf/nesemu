#include "mapper.h"
#include <stdio.h>
#include <stdlib.h>

u8 *Mapper::PRG;
u8 *Mapper::CHR;
size_t Mapper::sizePRG;
size_t Mapper::sizeCHR;

Mapper::~Mapper()
{
}

Mapper *Mapper::Load(const char *path)
{
	FILE *fp = fopen(path, "rb");

	if (!fp)
	{
		return NULL;
	}

	char id[4];
	fread(id, 1, 4, fp);

	if (id[0] != 'N' && id[1] != 'E' && id[2] != 'S' && id[3] != 0x1a)
	{
		return NULL;
	}

	u8 numPrgRom = fgetc(fp);
	u8 numChrRom = fgetc(fp);

	u8 flag = fgetc(fp);
	u8 mapper = (flag & 0xF0) >> 4;
	flag = fgetc(fp);
	mapper += (flag & 0xF0);

	printf("Mapper: %x\n", mapper);

	u8 numPrgRam = fgetc(fp);

	fseek(fp, 16L, SEEK_SET);

	sizePRG = numPrgRom * 16384;
	sizeCHR = numChrRom * 8192;

	if (PRG) delete[] PRG;
	if (CHR) delete[] CHR;

	PRG = new u8[sizePRG];
	CHR = new u8[sizeCHR];

	fread(PRG, 1, sizePRG, fp);
	fread(CHR, 1, sizeCHR, fp);

	if (mapper == 0) return new Mapper0;
	return nullptr;
}

u8 Mapper0::Read(u16 addr)
{
	if (addr < 0x2000)  { return CHR[addr]; }
	else if (addr >= 0x8000 && addr < 0x10000) { return PRG[addr - (0x10000 - sizePRG)]; }
	else 
	{
		// TODO printf
		exit(1);
	}
}