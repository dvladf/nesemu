#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "common.h"

enum Button
{
	ButtonA,
	ButtonB,
	ButtonSelect,
	ButtonStart,
	ButtonUp,
	ButtonDown,
	ButtonLeft,
	ButtonRight
};

class Controller
{
	bool buttons[8]{};
	u8 index = 0;
	u8 strobe = 0;

public:
	void SetButtons(bool *_buttons);
	u8   Read();
	void Write(u8 val);
};

#endif /* CONTROLLER_H */