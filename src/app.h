#ifndef APP_H
#define APP_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_syswm.h>
#include "cpu.h"

class App
{
	SDL_Window* window_;
	SDL_GLContext glContext_;
	SDL_SysWMinfo wmInfo_;
	GLuint texture;

	CPU* cpu;
	PPU* ppu;
	Mapper* mapper;
	Controller* controller1;
	Controller* controller2;
	bool buttons[8]{};

	float xCoord, yCoord;

public:
	App();
	~App();
	int Run(int argc, char** argv);

private:
	bool Init(int argc, char** argv);
	void Update(Uint32 msec);
	void Draw();
	int  Step();
	void StepSeconds(double sec);
	void OpenDialog();
	void OpenROM(const char *path);
	void ResizeWindow(int w, int h);
};

#endif /* APP_H */
