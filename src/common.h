#ifndef COMMON_H
#define COMMON_H

#include <cstdint>
#include <cstddef>

constexpr int WIDTH = 256;
constexpr int HEIGHT = 240;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#endif /* COMMON_H */