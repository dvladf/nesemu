#include "app.h"

#ifdef _WIN32
#include <Commdlg.h>
#endif

App::App()
{
	ppu = new PPU;
	controller1 = new Controller;
	controller2 = new Controller;
	cpu = new CPU(ppu, controller1, controller2);
	mapper = nullptr;
}

App::~App()
{
	delete ppu;
	delete controller1;
	delete controller2;
	delete cpu;
	if (mapper) { delete mapper; }

	SDL_DestroyWindow(window_);
	SDL_Quit();
}

int App::Run(int argc, char** argv)
{
#ifndef _WIN32
    if (argc < 2) {
        printf("Usage: NESemu <ROM file>\n");
        return 1;
    }
#endif

	if (!Init(argc, argv)) {
		return 1;
	}

	Uint32 prevTime = SDL_GetTicks();
	SDL_Event event;
	bool running = true;

	while (running) {
		glClear(GL_COLOR_BUFFER_BIT);

		uint32_t background = ppu->BackgroundColor();
		float r = ((background >> 16) & 0xFF) / 255.0f;
		float g = ( (background >> 8) & 0xFF ) / 255.0f;
		float b = (background & 0xFF) / 255.0f;
		glClearColor(r, g, b, 1.0f);
		
		Uint32 time = SDL_GetTicks();
		Uint32 dt = time - prevTime;
		prevTime = time;
		if (mapper) { Update(dt); }
		SDL_GL_SwapWindow(window_);

		while (SDL_PollEvent(&event)) {
			switch (event.type) {

			case SDL_QUIT:
				running = false;
				break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:	running = false;				break;
				case SDLK_q:		buttons[ButtonA] = true;		break;
				case SDLK_w:		buttons[ButtonB] = true;		break;
				case SDLK_s:		buttons[ButtonSelect] = true;	break;
				case SDLK_a:		buttons[ButtonStart] = true;	break;
				case SDLK_UP:		buttons[ButtonUp] = true;		break;
				case SDLK_DOWN:		buttons[ButtonDown] = true;		break;
				case SDLK_LEFT:		buttons[ButtonLeft] = true;		break;
				case SDLK_RIGHT:	buttons[ButtonRight] = true;	break;
				}
				break;

			case SDL_KEYUP:
				switch (event.key.keysym.sym) {
				case SDLK_q:		buttons[ButtonA] = false;		break;
				case SDLK_w:		buttons[ButtonB] = false;		break;
				case SDLK_s:		buttons[ButtonSelect] = false;	break;
				case SDLK_a:		buttons[ButtonStart] = false;	break;
				case SDLK_UP:		buttons[ButtonUp] = false;		break;
				case SDLK_DOWN:		buttons[ButtonDown] = false;	break;
				case SDLK_LEFT:		buttons[ButtonLeft] = false;	break;
				case SDLK_RIGHT:	buttons[ButtonRight] = false;	break;
				}
				break;

			case SDL_DROPFILE:
				OpenROM(event.drop.file);
				break;

			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					ResizeWindow(event.window.data1, event.window.data2);
					break;
				default:
					break;
				}
				break;
			}
		}

		controller1->SetButtons(buttons);
	}

	return 0;
}

bool App::Init(int argc, char **argv)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		return false;
	}

	window_ = SDL_CreateWindow("NESemu", SDL_WINDOWPOS_CENTERED, 
		SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	if (!window_) {
		return false;
	}

	SDL_VERSION(&wmInfo_.version);
	SDL_GetWindowWMInfo(window_, &wmInfo_);
	
	ResizeWindow(800, 600);
	glContext_ = SDL_GL_CreateContext(window_);

	SDL_EventState(SDL_DROPFILE, SDL_ENABLE);

	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glClear(GL_COLOR_BUFFER_BIT);
	

	SDL_GL_SwapWindow(window_);

	if (argc > 1) { OpenROM(argv[1]); }
	else { OpenDialog(); }
	
	return true;
}

void App::Update(Uint32 msec)
{
	double sec = msec * 0.001;

	if (sec > 1) { sec = 0; }

	StepSeconds(sec);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, WIDTH, HEIGHT, 0,
		GL_RGB, GL_UNSIGNED_BYTE, ppu->Buffer());
	Draw();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void App::Draw()
{
	glBegin(GL_QUADS);
	glTexCoord2f(0, 1); glVertex2f(-xCoord, -yCoord);
	glTexCoord2f(1, 1); glVertex2f( xCoord, -yCoord);
	glTexCoord2f(1, 0); glVertex2f( xCoord,  yCoord);
	glTexCoord2f(0, 0); glVertex2f(-xCoord,  yCoord);
	glEnd();
}

int App::Step()
{
	int cpuCycles = cpu->Step();
	int ppuCycles = cpuCycles * 3;

	for (int i = 0; i < ppuCycles; ++i) {
		ppu->Step(cpu);
	}

	return cpuCycles;
}

void App::StepSeconds(double sec)
{
	int cycles = static_cast<int>(CPUFrequency * sec);
	while (cycles > 0) {
		cycles -= Step();
	}
}

void App::OpenDialog()
{
#ifdef _WIN32
	OPENFILENAME ofn{};
	char szFile[MAX_PATH]{};
	
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = wmInfo_.info.win.window;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = "iNES files(*.nes)\0*.nes\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrInitialDir = ".\\roms";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&ofn)) {
		OpenROM(ofn.lpstrFile);
	}
#endif
}

void App::OpenROM(const char *path)
{
	Mapper *newMapper = Mapper::Load(path);
	
	if (newMapper) {

		if (mapper) 
			delete mapper;

		mapper = newMapper;
		cpu->SetMapper(mapper);
		ppu->SetMapper(mapper);
		cpu->Reset();
	}
}

void App::ResizeWindow(int w, int h)
{
	float s1 = static_cast<float>(w) / WIDTH;
	float s2 = static_cast<float>(h) / HEIGHT;

	if (s1 >= s2) {
		xCoord = s2 / s1;
		yCoord = 1;
	} else {
		xCoord = 1;
		yCoord = s1 / s2;
	}

	glViewport(0, 0, w, h);
}
