#include "cpu.h"
#include <stdio.h>
#include <stdlib.h>

CPU::CPU(PPU *ppu, Controller *controller1, Controller *controller2) :
	ppu(ppu), controller1(controller1), controller2(controller2)
{
	A = S = X = Y = 0x00;
	interrupt = Interrupt::Nothing;
}

void CPU::Reset()
{
	PC = Read16(0xFFFC);
	S = 0xFD;
	P.set(0x24);
}

int CPU::Step()
{
	if (stall > 0)
	{
		stall--;
		return 1;
	}

	u64 prevCycles = cycles;

	switch (interrupt)
	{
	case CPU::Interrupt::Nothing:
		break;
	case CPU::Interrupt::NMI:
		NMI();
		break;
	case CPU::Interrupt::IRQ:
		IRQ();
		break;
	}

	interrupt = Interrupt::Nothing;

	// Interrupt handlers
	opcode = Read(PC);
	mode   = instModes[opcode];

	u16 offset = 0;
	bool pageCrossed = false;

	switch (mode)
	{
	case ABS:
		addr = Read16(PC + 1);
		break;
	case ABSX:
		addr = Read16(PC + 1) + static_cast<u16>(X);
		pageCrossed = PagesDiff(addr - static_cast<u16>(X), addr);
		break;
	case ABSY:
		addr = Read16(PC + 1) + static_cast<u16>(Y);
		pageCrossed = PagesDiff(addr - static_cast<u16>(Y), addr);
		break;
	case ACC:
		addr = 0;
		break;
	case IMM:
		addr = PC + 1;
		break;
	case IMPL:
		addr = 0;
		break;
	case INDX:
		addr = Read16Bug(static_cast<u16>(Read(PC + 1) + X));
		break;
	case IND:
		addr = Read16Bug(Read16(PC + 1));
		break;
	case INDY:
		addr = Read16Bug(static_cast<u16>(Read(PC + 1))) + static_cast<u16>(Y);
		pageCrossed = PagesDiff(addr - static_cast<u16>(Y), addr);
		break;
	case REL:
		offset = static_cast<u16>(Read(PC + 1));
		if (offset < 0x80)	{ addr = PC + 2 + offset; }
		else				{ addr = PC + 2 + offset - 0x100; }
		break;
	case ZP:
		addr = static_cast<u16>(Read(PC + 1));
		break;
	case ZPX:
		addr = static_cast<u16>(Read(PC + 1) + X);
		break;
	case ZPY:
		addr = static_cast<u16>(Read(PC + 1) + Y);
		break;
	}

	//DebugOutput();

	PC += static_cast<u16>(instSizes[opcode]);
	cycles += static_cast<u64>(instCycles[opcode]);

	if (pageCrossed)
	{
		cycles += static_cast<u64>(instPageCycles[opcode]);
	}

	(this->*instTable[opcode])();
	return static_cast<int>(cycles - prevCycles);
}

void CPU::TriggerNMI()
{
	interrupt = Interrupt::NMI;
}

void CPU::TriggerIRQ()
{
	if (P[I] == 0)
	{
		interrupt = Interrupt::IRQ;
	}
}

void CPU::NMI()
{
	Push16(PC);
	PHP();
	PC = Read16(0xFFFA);
	P[I] = 1;
	cycles += 7;
}

void CPU::IRQ()
{
	Push16(PC);
	PHP();
	PC = Read16(0xFFFE);
	P[I] = 1;
	cycles += 7;
}

void CPU::DebugOutput()
{
	printf("%x:", PC);
	
	for (int i = 0; i < instSizes[opcode]; ++i)
	{
		printf("%x ", Read(PC + i));
	}

	printf("\t%s", instNames[opcode]);
	printf("\t%x\n", A);
}

u8 CPU::Read(u16 addr)
{
	if (addr < 0x2000) { return RAM[addr % 0x0800]; }
	else if (addr >= 0x2000 && addr < 0x4000) { return ppu->ReadRegister(addr % 8); }
	else if (addr >= 0x4000 && addr < 0x4014 ) { /* APU */ }
	else if (addr == 0x4016) { return controller1->Read(); }
	else if (addr == 0x4017) { return controller2->Read(); }
	else if (addr >= 0x4018 && addr < 0x6000) { /* IO Registers*/ }
	else if (addr >= 0x8000 && addr < 0x10000) { return mapper->Read(addr); }
	else { exit(1); }
	return 0;
}

void CPU::Write(u16 addr, u8 val)
{
	if (addr < 0x2000) { RAM[addr % 0x0800] = val; }
	else if (addr >= 0x2000 && addr < 0x4000) { ppu->WriteRegister(addr % 8, val); }
	else if (addr >= 0x4000 && addr < 0x4014) { /* APU */ }
	else if (addr == 0x4014) { ppu->WriteDMA(val, this, cycles, &stall); }
	else if (addr == 0x4015) { /* APU */ }
	else if (addr == 0x4016) { controller1->Write(val); controller2->Write(val); }
	else if (addr == 0x4017) { /* APU */ }
	else if (addr >= 0x4018 && addr < 0x6000) { /* IO Registers*/ }
	else if (addr >= 0x8000 && addr < 0x10000)  { /* mapper; */ }
	else { exit(2); }
}

u16 CPU::Read16(u16 addr)
{
	u16 lo = static_cast<u16>(Read(addr));
	u16 hi = static_cast<u16>(Read(addr + 1));
	return (hi << 8) | lo;
}

u16 CPU::Read16Bug(u16 addr)
{
	u16 a = addr;
	u16 b = (a & 0xFF00) | static_cast<u16>((static_cast<u8>(a) + 1));
	u8 lo = Read(a);
	u8 hi = Read(b);
	return (static_cast<u16>(hi) << 8) | static_cast<u16>(lo);
}

void CPU::Push16(u16 val)
{
	u8 hi = static_cast<u8>(val >> 8);
	u8 lo = static_cast<u8>(val & 0xFF);
	Push(hi);
	Push(lo);
}

u16 CPU::Pull16()
{
	u16 lo = static_cast<u16>(Pull());
	u16 hi = static_cast<u16>(Pull());
	return (hi << 8) | lo;
}

void CPU::SetZ(u8 val)
{
	if (val == 0)	P[Z] = 1;
	else			P[Z] = 0;
}

void CPU::SetN(u8 val)
{
	if ((val & 0x80) != 0)	P[N] = 1;
	else					P[N] = 0;
}

void CPU::Compare(u8 a, u8 b)
{
	SetZN(a - b);
	if (a >= b) P[C] = 1;
	else		P[C] = 0;
}

void CPU::AddBranchCycles()
{
	cycles++;
	if (PagesDiff(PC, addr))
		cycles++;
}

/***************** Legal instructions *****************/

// ADC - Add with Carry
void CPU::ADC()
{
	u8 a = A;
	u8 b = Read(addr);
	u8 c = P[C];

	A = a + b + c;
	SetZN(A);

	if (static_cast<int>(a) + static_cast<int>(b) + static_cast<int>(c) > 0xFF)
		P[C] = 1;
	else
		P[C] = 0;

	if (((a^b) & 0x80) == 0 && ((a^A) & 0x80) != 0)
		P[V] = 1;
	else
		P[V] = 0;
}

// AND - Logical AND
void CPU::AND()
{
	A = A & Read(addr);
	SetZN(A);
}

// ASL - Arithmetic Shift Left
void CPU::ASL()
{
	if (mode == ACC)
	{
		P[C] = (A >> 7) & 1;
		A <<= 1;
		SetZN(A);
	}
	else
	{
		u8 val = Read(addr);
		P[C] = (val >> 7) & 1;
		val <<= 1;
		Write(addr, val);
		SetZN(val);
	}
}

// BCC - Branch if Carry Clear
void CPU::BCC()
{
	if (P[C] == 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BCS - Branch if Carry Set
void CPU::BCS()
{
	if (P[C] != 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BEQ - Branch if Equal
void CPU::BEQ()
{
	if (P[Z] != 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BIT - Bit Test
void CPU::BIT()
{
	u8 val = Read(addr);
	P[V] = (val >> 6) & 1;
	SetZ(val & A);
	SetN(val);
}

// BMI - Branch if Minus
void CPU::BMI()
{
	if (P[N] != 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BNE - Branch if Not Equal
void CPU::BNE()
{
	if (P[Z] == 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BPL - Branch if Positive
void CPU::BPL()
{
	if (P[N] == 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BRK - Force Interrupt
void CPU::BRK()
{
	Push16(PC);
	PHP();
	SEI();
	PC = Read16(0xFFFE);
}

// BVC - Branch if Overflow Clear
void CPU::BVC()
{
	if (P[V] == 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// BVS - Branch if Overflow Set
void CPU::BVS()
{
	if (P[V] != 0)
	{
		PC = addr;
		AddBranchCycles();
	}
}

// CLC - Clear Carry Flag
void CPU::CLC()
{
	P[C] = 0;
}

// CLC - Clear Decimal Mode
void CPU::CLD()
{
	P[D] = 0;
}

// CLI - Clear Interrupt Disable
void CPU::CLI()
{
	P[I] = 0;
}

// CLV - Clear Overflow Flag 
void CPU::CLV()
{
	P[V] = 0;
}

// CMP - Compare
void CPU::CMP()
{
	u8 val = Read(addr);
	Compare(A, val);
}

// CPX - Compare X Register
void CPU::CPX()
{
	u8 val = Read(addr);
	Compare(X, val);
}

// CPY - Compare Y Register
void CPU::CPY()
{
	u8 val = Read(addr);
	Compare(Y, val);
}

// DEC - Decrement Memory
void CPU::DEC()
{
	u8 val = Read(addr) - 1;
	Write(addr, val);
	SetZN(val);
}

// DEX - Decrement X Register
void CPU::DEX()
{
	X--;
	SetZN(X);
}

// DEY - Decrement Y Register
void CPU::DEY()
{
	Y--;
	SetZN(Y);
}

// EOR - Exclusive OR
void CPU::EOR()
{
	A = A ^ Read(addr);
	SetZN(A);
}

// INC - Increment Memory
void CPU::INC()
{
	u8 val = Read(addr) + 1;
	Write(addr, val);
	SetZN(val);
}

// INX - Increment X Register
void CPU::INX()
{
	X++;
	SetZN(X);
}

// INY - Increment Y Register
void CPU::INY()
{
	Y++;
	SetZN(Y);
}

// JMP - Jump
void CPU::JMP()
{
	PC = addr;
}

// JSR - Jump to Subroutine
void CPU::JSR()
{
	Push16(PC - 1);
	PC = addr;
}

// LDA - Load Accumulator
void CPU::LDA()
{
	A = Read(addr);
	SetZN(A);
}

// LDX - Load X Register
void CPU::LDX()
{
	X = Read(addr);
	SetZN(X);
}

// LDY - Load Y Register
void CPU::LDY()
{
	Y = Read(addr);
	SetZN(Y);
}

// LSR - Logical Shift Right
void CPU::LSR()
{
	if (mode == ACC)
	{
		P[C] = A & 1;
		A >>= 1;
		SetZN(A);
	}
	else
	{
		u8 val = Read(addr);
		P[C] = val & 1;
		val >>= 1;
		Write(addr, val);
		SetZN(val);
	}
}

// NOP - No operation
void CPU::NOP()
{
}

// ORA - Logical Inclusive OR
void CPU::ORA()
{
	A = A | Read(addr);
	SetZN(A);
}

// PHA - Push Accumulator
void CPU::PHA()
{
	Push(A);
}

// PHP - Push Processor Status
void CPU::PHP()
{
	Push(P.get() | 0x10);
}

// PLA - Pull Accumulator
void CPU::PLA()
{
	A = Pull();
	SetZN(A);
}

// PLP - Pull Processor Status
void CPU::PLP()
{

	P.set(Pull() & 0xEF | 0x20);
}

// ROL - Rotate Left
void CPU::ROL()
{
	if (mode == ACC)
	{
		u8 c = P[C];
		P[C] = (A >> 7) & 1;
		A = (A << 1) | c;
		SetZN(A);
	}
	else
	{
		u8 c = P[C];
		u8 val = Read(addr);
		P[C] = (val >> 7) & 1;
		val = (val << 1) | c;
		Write(addr, val);
		SetZN(val);
	}
}

// ROR - Rotate Right
void CPU::ROR()
{
	if (mode == ACC)
	{
		u8 c = P[C];
		P[C] = A & 1;
		A = (A >> 1) | (c << 7);
		SetZN(A);
	}
	else
	{
		u8 c = P[C];
		u8 val = Read(addr);
		P[C] = val & 1;
		val = (val >> 1) | (c << 7);
		Write(addr, val);
		SetZN(val);
	}
}

// RTI - Return from Interrupt
void CPU::RTI()
{
	P.set(Pull() & 0xEF | 0x20);
	PC = Pull16();
}

// RTS - Return from Subroutine
void CPU::RTS()
{
	PC = Pull16() + 1;
}

// SBC - Subtract with Carry
void CPU::SBC()
{
	u8 a = A;
	u8 b = Read(addr);
	u8 c = P[C];
	A = a - b - (1 - c);
	SetZN(A);

	if ((static_cast<int>(a) - static_cast<int>(b) - static_cast<int>(1-c)) >= 0)
		P[C] = 1;
	else
		P[C] = 0;

	if (((a^b) & 0x80) != 0 && ((a^A) & 0x80) != 0)
		P[V] = 1;
	else
		P[V] = 0;
}

// SEC - Set Carry Flag
void CPU::SEC()
{
	P[C] = 1;
}

// SED - Set Decimal Flag
void CPU::SED()
{
	P[D] = 1;
}

// SEI - Set Interrupt Disable
void CPU::SEI()
{
	P[I] = 1;
}

// STA - Store Accumulator
void CPU::STA()
{
	Write(addr, A);
}

// STX - Store X Register
void CPU::STX()
{
	Write(addr, X);
}

// STY - Store Y Register
void CPU::STY()
{
	Write(addr, Y);
}

// TAX - Transfer Accumulator to X
void CPU::TAX()
{
	X = A;
	SetZN(X);
}

// TAY - Transfer Accumulator to Y
void CPU::TAY()
{
	Y = A;
	SetZN(Y);
}

// TSX - Transfer Stack to X
void CPU::TSX()
{
	X = S;
	SetZN(X);
}

// TXA - Transfer X to Accumulator
void CPU::TXA()
{
	A = X;
	SetZN(A);
}

// TXS - Transfer X to Stack Pointer
void CPU::TXS()
{
	S = X;
}

// TYA - Transfer Y to Accumulator
void CPU::TYA()
{
	A = Y;
	SetZN(A);
}

// Illegal instructions

void CPU::AHX()
{
}

void CPU::ALR()
{
}

void CPU::ANC()
{
}

void CPU::ARR()
{
}

void CPU::AXS()
{
}

void CPU::DCP()
{
}

void CPU::ISC()
{
}

void CPU::KIL()
{
}

void CPU::LAS()
{
}

void CPU::LAX()
{
}

void CPU::RLA()
{
}

void CPU::RRA()
{
}

void CPU::SAX()
{
}

void CPU::SHX()
{
}

void CPU::SHY()
{
}

void CPU::SLO()
{
}

void CPU::SRE()
{
}

void CPU::TAS()
{
}

void CPU::XAA()
{
}