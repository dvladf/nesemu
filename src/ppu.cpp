#include "ppu.h"
#include "cpu.h"
#include <stdlib.h>

PPU::PPU()
{
	back = new u8[WIDTH * HEIGHT * 3];
	front = new u8[WIDTH * HEIGHT * 3];
	Reset();
}

PPU::~PPU()
{
	delete[] back;
	delete[] front;
}

void PPU::Reset()
{
	cycle = 340;
	scanLine = 240;
	frame = 0;
	WriteControl(0);
	WriteMask(0);
	WriteOAMAddress(0);
}

u8 PPU::Read(u16 addr)
{
	addr %= 0x4000;

	if (addr < 0x2000) { return mapper->Read(addr); }
	else if (addr >= 0x2000 && addr < 0x2800) { return nameTableData[addr - 0x2000]; }
	else if (addr >= 0x2800 && addr < 0x3000) { return nameTableData[addr - 0x2800]; }
	else if (addr >= 0x3000 && addr < 0x3800) { return nameTableData[addr - 0x3000]; }
	else if (addr >= 0x3800 && addr < 0x3F00) { return nameTableData[addr - 0x3800]; }
	else if (addr >= 0x3F00 && addr < 0x4000) { return ReadPallete(addr % 32); }
	else { exit(10); }

	return 0;
}


void PPU::Write(u16 addr, u8 val)
{
	addr %= 0x4000;

	if (addr < 0x2000) { return; }
	else if (addr >= 0x2000 && addr < 0x2800) { nameTableData[addr - 0x2000] = val; }
	else if (addr >= 0x2800 && addr < 0x3000) { nameTableData[addr - 0x2800] = val; }
	else if (addr >= 0x3000 && addr < 0x3800) { nameTableData[addr - 0x3000] = val; }
	else if (addr >= 0x3800 && addr < 0x3F00) { nameTableData[addr - 0x3800] = val; }
	else if (addr >= 0x3F00 && addr < 0x4000) { WritePallete(addr % 32, val); }
	else { exit(11); }
}


u8 PPU::ReadPallete(u16 addr)
{
	if (addr >= 16 && addr % 4 == 0)
		addr -= 16;

	return palleteData[addr];
}


void PPU::WritePallete(u16 addr, u8 val)
{
	if (addr >= 16 && addr % 4 == 0)
		addr -= 16;

	palleteData[addr] = val;
}


u8 PPU::ReadRegister(u16 addr)
{
	switch (addr)
	{
	case 0x2: return ReadStatus();
	case 0x4: return ReadOAMData();
	case 0x7: return ReadData();
	}
	return 0;
}


void PPU::WriteRegister(u16 addr, u8 val)
{
	reg = val;
	switch (addr)
	{
	case 0x0:		WriteControl(val);		break;
	case 0x1:		WriteMask(val);			break;
	case 0x3:		WriteOAMAddress(val);	break;
	case 0x4:		WriteOAMData(val);		break;
	case 0x5:		WriteScroll(val);		break;
	case 0x6:		WriteAddress(val);		break;
	case 0x7:		WriteData(val);			break;
	}
}


// $4041: OAMDMA
void PPU::WriteDMA(u8 val, CPU *cpu, u64 cpuCycles, int *cpuStall)
{
	u16 addr = static_cast<u16>(val) << 8;
	for (u16 i = 0; i < 256; ++i)
	{
		oamData[oamAddr] = cpu->Read(addr);
		oamAddr++;
		addr++;
	}

	(*cpuStall) += 513;
	if (cpuCycles % 2 == 1)
		(*cpuStall)++;
}

// $2000: PPUCTRL
void PPU::WriteControl(u8 val)
{
	fNameTable			= (val >> 0) & 3;
	fIncrement			= (val >> 2) & 1;
	fSpriteTable		= (val >> 3) & 1;
	fBackgroundTable	= (val >> 4) & 1;
	fSpriteSize			= (val >> 5) & 1;
	fMasterSlave		= (val >> 6) & 1;
	nmiOutput = (((val >> 7) & 1) == 1);
	ChangeNMI();
	// t: ....BA.. ........ = d: ......BA
	_t = (_t & 0xF3FF) | ((static_cast<u16>(val) & 0x03) << 10);
}

// $2001: PPUMASK
void PPU::WriteMask(u8 val)
{
	fGrayscale			= (val >> 0) & 1;
	fShowLeftBackground = (val >> 1) & 1;
	fShowLeftSprites	= (val >> 2) & 1;
	fShowBackground		= (val >> 3) & 1;
	fShowSprites		= (val >> 4) & 1;
	fRedTint			= (val >> 5) & 1;
	fGreenTint			= (val >> 6) & 1;
	fBlueTint			= (val >> 7) & 1;
}

// $2002: PPUSTATUS
u8 PPU::ReadStatus()
{
	u8 result = reg & 0x1F;
	result |= fSpriteOverflow << 5;
	result |= fSpriteZeroHit  << 6;

	if (nmiOccured)
		result |= 1 << 7;

	nmiOccured = false;
	ChangeNMI();
	// w:                   = 0
	_w = 0;
	return result;
}

// $2003: OAMADDR
void PPU::WriteOAMAddress(u8 val)
{
	oamAddr = val;
}

// $2004: OAMDATA (read)
u8 PPU::ReadOAMData()
{
	return oamData[oamAddr];
}

// $2004: OAMDATA (write)
void PPU::WriteOAMData(u8 val)
{
	oamData[oamAddr] = val;
	oamAddr++;
}

// $2005: PPUSCROLL
void PPU::WriteScroll(u8 val)
{
	if (_w == 0)
	{
		// t: ........ ...HGFED = d: HGFED...
		// x:               CBA = d: .....CBA
		// w:                   = 1
		_t = (_t & 0xFFE0) | (static_cast<u16>(val) >> 3);
		_x = val & 0x07;
		_w = 1;
	}
	else
	{
		// t: .CBA..HG FED..... = d: HGFEDCBA
		// w:                   = 0
		_t = (_t & 0x8FFF) | ((static_cast<u16>(val) & 0x07) << 12);
		_t = (_t & 0xFC1F) | ((static_cast<u16>(val) & 0xF8) << 2);
		_w = 0;
	}
}

// $2006: PPUADDR
void PPU::WriteAddress(u8 val)
{
	if (_w == 0)
	{
		// t: ..FEDCBA ........ = d: ..FEDCBA
		// t: .X...... ........ = 0
		// w:                   = 1
		_t = (_t & 0x80FF) | ((static_cast<u16>(val) & 0x3F) << 8);
		_w = 1;
	}
	else
	{
		// t: ........ HGFEDCBA = d: HGFEDCBA
		// v                    = t
		// w:                   = 0
		_t = (_t & 0xFF00) | static_cast<u16>(val);
		_v = _t;
		_w = 0;
	}
}

// $2007: PPUDATA (read)
u8 PPU::ReadData()
{
	u8 val = Read(_v);
	if ((_v % 0x4000) < 0x3F00)
	{
		u8 buffered = bufferedData;
		bufferedData = val;
		val = buffered;
	}
	else
	{
		bufferedData = Read(_v - 0x1000);
	}

	if (fIncrement == 0)
		_v += 1;
	else
		_v += 32;

	return val;
}

// $2007: PPUDATA (write)
void PPU::WriteData(u8 val)
{
	Write(_v, val);
	if (fIncrement == 0)
		_v += 1;
	else
		_v += 32;
}


void PPU::IncX()
{
	// increment hori(v)
	// if coarse X == 31
	if ((_v & 0x001F) == 31)
	{
		// coarse X = 0
		_v &= 0xFFE0;
		// switch horizontal nametable
		_v ^= 0x0400;
	}
	else
		_v++;
}


void PPU::IncY()
{
	// increment vert(v)
	// if fine Y < 7
	if ((_v & 0x7000) != 0x7000)
	{
		_v += 0x1000;
	}
	else
	{
		// fine Y = 0
		_v &= 0x8FFF;
		// let y = coarse Y
		u16 y = (_v & 0x3E0) >> 5;
		if (y == 29)
		{
			y = 0;			// coarse Y = 0
			_v ^= 0x0800;	// switch vertical nametable
		}
		else if (y == 31)
		{
			y = 0;			// coarse Y = 0, nametable not switched
		}
		else
		{
			y++;			// increment coarse Y
		}
		// put coarse Y back into v 
		_v = (_v & 0xFC1F) | (y << 5);
	}
}


void PPU::CopyX()
{
	// hori(v) = hori(t)
	// v: .....F.. ...EDCBA = t: .....F.. ...EDCBA
	_v = (_v & 0xFBE0) | (_t & 0x041F);
}


void PPU::CopyY()
{
	// vert(v) = vert(t)
	// v: .IHGF.ED CBA..... = t: .IHGF.ED CBA.....
	_v = (_v & 0x841F) | (_t & 0x7BE0);
}


void PPU::ChangeNMI()
{
	bool nmi = nmiOutput && nmiOccured;
	if (nmi && !nmiPrev)
	{
		nmiDelay = 15;
	}
	nmiPrev = nmi;
}


void PPU::SetVertBlank()
{
	u8 *s = back;
	back = front;
	front = s;

	nmiOccured = true;
	ChangeNMI();
}


void PPU::ClearVertBlank()
{
	nmiOccured = false;
	ChangeNMI();
}


void PPU::FetchNameTableByte()
{
	u16 v = _v;
	u16 addr = 0x2000 | (v & 0x0FFF);
	nameTableByte = Read(addr);
}


void PPU::FetchAttrTableByte()
{
	u16 v = _v;
	u16 addr = 0x23C0 | (v & 0x0C00) | ((v >> 4) & 0x38) | ((v >> 2) & 0x07);
	u16 shift = ((v >> 4) & 4) | (v & 2);
	attribTableByte = ((Read(addr) >> shift) & 3) << 2;
}


void PPU::FetchLoTileByte()
{
	u16 fineY = (_v >> 12) & 7;
	u8 table = fBackgroundTable;
	u8 tile  = nameTableByte;
	u16 addr = 0x1000 * static_cast<u16>(table) + static_cast<u16>(tile) * 16 + fineY;
	loTileByte = Read(addr);
}


void PPU::FetchHiTileByte()
{
	u16 fineY = (_v >> 12) & 7;
	u8 table = fBackgroundTable;
	u8 tile = nameTableByte;
	u16 addr = 0x1000 * static_cast<u16>(table) + static_cast<u16>(tile) * 16 + fineY;
	hiTileByte = Read(addr + 8);
}


void PPU::StoreTileData()
{
	u32 data = 0;

	for (u8 i = 0; i < 8; ++i)
	{
		u8 a = attribTableByte;
		u8 p1 = (loTileByte & 0x80) >> 7;
		u8 p2 = (hiTileByte & 0x80) >> 6;
		loTileByte <<= 1;
		hiTileByte <<= 1;
		data <<= 4;
		data |= static_cast<u32>(a | p1 | p2);
	}
	tileData |= static_cast<u64>(data);
}

u32 PPU::FetchTileData()
{
	return static_cast<u32>(tileData >> 32);
}


u8 PPU::BackgroundPixel()
{
	if (fShowBackground == 0)
		return 0;
	u32 data = FetchTileData() >> ((7 - _x) * 4);
	return static_cast<u8>(data & 0x0F);
}


u8 PPU::SpritePixel(u8 &index)
{
	if (fShowSprites == 0)
	{
		index = 0;
		return 0;
	}

	for (int i = 0; i < spriteCount; ++i)
	{
		int offset = (cycle - 1) - static_cast<int>(spritePositions[i]);
		if (offset < 0 || offset > 7)
			continue;

		offset = 7 - offset;
		u8 color = static_cast<u8>((spritePatterns[i] >> static_cast<u8>(offset * 4)) & 0x0F);

		if (color % 4 == 0)
			continue;

		index = static_cast<u8>(i);
		return color;
	}

	index = 0;
	return 0;
}


void PPU::RenderPixel()
{
	int x = cycle - 1;
	int y = scanLine;

	u8 i = 0;
	u8 background = BackgroundPixel();
	u8 sprite = SpritePixel(i);

	if (x < 8 && fShowLeftBackground == 0)
		background = 0;

	if (x < 8 && fShowLeftSprites == 0)
		sprite = 0;

	bool b = (background % 4 != 0);
	bool s = (sprite % 4 != 0);

	u8 color;

	if (!b && !s)
		color = 0;
	else if (!b && s)
		color = sprite | 0x10;
	else if (b && !s)
		color = background;
	else
	{
		if (spriteIndexes[i] == 0 && x < 255)
			fSpriteZeroHit = 1;

		if (spritePriorities[i] == 0)
			color = sprite | 0x10;
		else
			color = background;
	}

	u8 c = ReadPallete(static_cast<u16>(color) % 64);
	SetPixel(x, y, c);
}

u32 PPU::FetchSpritePattern(int i, int row)
{
	u8 tile = oamData[i * 4 + 1];
	u8 attr = oamData[i * 4 + 2];

	u16 addr = 0;

	if (fSpriteSize == 0)
	{
		if ((attr & 0x80) == 0x80)
			row = 7 - row;

		u8 table = fSpriteTable;
		addr = 0x1000 * static_cast<u16>(table) + static_cast<u16>(tile) * 16 + static_cast<u16>(row);
	}
	else
	{
		if ((attr & 0x80) == 0x80)
			row = 15 - row;

		u8 table = tile & 1;
		tile &= 0xFE;
		if (row > 7)
		{
			tile++;
			row -= 8;
		}
		addr = 0x1000 * static_cast<u16>(table) + static_cast<u16>(tile) * 16 + static_cast<u16>(row);
	}

	u8 a = (attr & 3) << 2;
	loTileByte = Read(addr);
	hiTileByte = Read(addr + 8);

	u32 data = 0;
	for (int i = 0; i < 8; ++i)
	{
		u8 p1, p2;
		if ((attr & 0x40) == 0x40)
		{
			p1 = (loTileByte & 1) << 0;
			p2 = (hiTileByte & 1) << 1;
			loTileByte >>= 1;
			hiTileByte >>= 1;
		}
		else
		{
			p1 = (loTileByte & 0x80) >> 7;
			p2 = (hiTileByte & 0x80) >> 6;
			loTileByte <<= 1;
			hiTileByte <<= 1;
		}
		data <<= 4;
		data |= static_cast<u32>(a | p1 | p2);
	}

	return data;
}


void PPU::EvaluateSprites()
{
	int h;
	if (fSpriteSize == 0)
		h = 8;
	else
		h = 16;

	int count = 0;
	for (int i = 0; i < 64; ++i)
	{
		u8 y = oamData[i * 4 + 0];
		u8 a = oamData[i * 4 + 2];
		u8 x = oamData[i * 4 + 3];
		int row = scanLine - static_cast<int>(y);
		
		if (row < 0 || row >= h)
			continue;

		if (count < 8)
		{
			spritePatterns[count] = FetchSpritePattern(i, row);
			spritePositions[count] = x;
			spritePriorities[count] = (a >> 5) & 1;
			spriteIndexes[count] = static_cast<u8>(i);
		}
		count++;
	}

	if (count > 8)
	{
		count = 8;
		fSpriteOverflow = 1;
	}

	spriteCount = count;
}


void PPU::Tick(CPU *cpu)
{
	if (nmiDelay > 0)
	{
		nmiDelay--;
		if (nmiDelay == 0 && nmiOutput && nmiOccured)
			cpu->TriggerNMI();
	}

	if (fShowBackground != 0 || fShowSprites != 0)
	{
		if (_f == 1 && scanLine == 261 && cycle == 339)
		{
			cycle = 0;
			scanLine = 0;
			frame++;
			_f ^= 1;
			return;
		}
	}

	cycle++;
	if (cycle > 340)
	{
		cycle = 0;
		scanLine++;
		if (scanLine > 261)
		{
			scanLine = 0;
			frame++;
			_f ^= 1;
		}
	}
}

void PPU::Step(CPU *cpu)
{
	PPU::Tick(cpu);

	bool renderingEnabled = (fShowBackground != 0 || fShowSprites != 0);
	bool preLine = (scanLine == 261);
	bool visibleLine = (scanLine < 240);
	bool renderLine = (preLine || visibleLine);
	bool preFetchCycle = (cycle >= 321 && cycle <= 336);
	bool visibleCycle = (cycle >= 1 && cycle <= 256);
	bool fetchCycle = (preFetchCycle || visibleCycle);

	// background logic
	if (renderingEnabled)
	{
		if (visibleLine && visibleCycle)
			RenderPixel();

		if (renderLine && fetchCycle)
		{
			tileData <<= 4;
			switch (cycle % 8)
			{
			case 1: FetchNameTableByte();	break;
			case 3: FetchAttrTableByte();	break;
			case 5: FetchLoTileByte();		break;
			case 7: FetchHiTileByte();		break;
			case 0: StoreTileData();		break;
			}
		}

		if (preLine && cycle >= 280 && cycle <= 304)
			CopyY();

		if (renderLine)
		{
			if (fetchCycle && (cycle % 8 == 0))
				IncX();
			if (cycle == 256)
				IncY();
			if (cycle == 257)
				CopyX();
		}
	}


	// sprite logic
	if (renderingEnabled)
	{
		if (cycle == 257)
		{
			if (visibleLine)
				EvaluateSprites();
			else
				spriteCount = 0;
		}
	}

	// vblank logic
	if (scanLine == 241 && cycle == 1)
		SetVertBlank();

	if (preLine && cycle == 1)
	{
		ClearVertBlank();
		fSpriteZeroHit = 0;
		fSpriteOverflow = 0;
	}
}


void PPU::SetPixel(int x, int y, u8 c)
{
	u32 color = colors[c];
	int pos = 3*x + 3*WIDTH*y;
	back[pos + 0] = static_cast<u8>(color >> 16);
	back[pos + 1] = static_cast<u8>(color >> 8);
	back[pos + 2] = static_cast<u8>(color);
}