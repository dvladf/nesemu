#ifndef PPU_H
#define PPU_H

#include "mapper.h"

class CPU;

class PPU
{
	u8 *back;
	u8 *front;

	int cycle;		// 0-340
	int scanLine;	// 0-261, 0-239=visible, 240=post, 241-260=vblank, 261=pre 
	u64 frame;		// frame counter

	// Memory
	u8 palleteData[32]{};
	u8 nameTableData[2048]{};
	u8 oamData[256]{};

	// PPU internal register
	u16 _v = 0;	// current vram address (15 bit)
	u16 _t = 0;	// temporary vram address (15 bit)
	u8	_x = 0;	// fine x scroll (3 bit)
	u8	_w = 0;	// write toogle (1 bit)
	u8  _f = 0; // even/odd frame flag (1 bit)

	u8 reg;

	// NMI
	bool	nmiOccured = false;
	bool	nmiOutput = false;
	bool	nmiPrev = false;
	u8		nmiDelay = 0;

	// background temporary variables
	u8  nameTableByte = 0;
	u8  attribTableByte = 0;
	u8  loTileByte = 0;
	u8  hiTileByte = 0;
	u64 tileData = 0;

	// sprite temporary variables
	int spriteCount = 0;
	
	u32 spritePatterns[8]{};
	u8  spritePositions[8]{};
	u8  spritePriorities[8]{};
	u8  spriteIndexes[8]{};

	// $2000 PPUCTRL
	u8 fNameTable;			// 0: $2000; 1: $2400; 2: $2800; 3: $2C00
	u8 fIncrement;			// 0: add 1; 1: add 32
	u8 fSpriteTable;		// 0: $0000; 1: $1000; ignored in 8x16 mode
	u8 fBackgroundTable;
	u8 fSpriteSize;			// 0: 8x8; 1: 8x16
	u8 fMasterSlave;		// 0: read EXT; 1: write EXT

	// $2001 PPUMASK
	u8 fGrayscale;			// 0: color; 1: grayscale
	u8 fShowLeftBackground;	// 0: hide; 1: show
	u8 fShowLeftSprites;	// 0: hide; 1: show
	u8 fShowBackground;		// 0: hide; 1: show
	u8 fShowSprites;		// 0: hide; 1: show
	u8 fRedTint;			// 0: normal; 1: emphasized
	u8 fGreenTint;			// 0: normal; 1: emphasized
	u8 fBlueTint;			// 0: normal; 1: emphasized

	// $2002 PPUSTATUS
	u8 fSpriteZeroHit;
	u8 fSpriteOverflow;

	// $2003 OAMADDR
	u8 oamAddr;

	// $2007 PPUDATA
	u8 bufferedData;

	Mapper *mapper;

public:
	PPU();
	~PPU();
	void Reset();
	u8   ReadRegister(u16 addr);
	void WriteRegister(u16 addr, u8 val);
	void WriteDMA(u8 val, CPU *cpu, u64 cpuCycles, int *cpuStall);
	void Step(CPU *cpu);
	u64  Frame() { return frame; }
	void SetMapper(Mapper *_mapper) { mapper = _mapper; }
	const u8 *Buffer() { return front; }
	u32 BackgroundColor() { return colors[palleteData[0]]; }

private:
	u8   Read(u16 addr);
	void Write(u16 addr, u8 val);

	u8   ReadPallete(u16 addr);
	void WritePallete(u16 addr, u8 val);

	void WriteControl(u8 val);
	void WriteMask(u8 val);
	u8   ReadStatus();
	void WriteOAMAddress(u8 val);
	u8   ReadOAMData();
	void WriteOAMData(u8 val);
	void WriteScroll(u8 val);
	void WriteAddress(u8 val);
	u8   ReadData();
	void WriteData(u8 val);

	void IncX();
	void IncY();
	void CopyX();
	void CopyY();

	void ChangeNMI();
	void SetVertBlank();
	void ClearVertBlank();
	void FetchNameTableByte();
	void FetchAttrTableByte();
	void FetchLoTileByte();
	void FetchHiTileByte();
	void StoreTileData();
	u32  FetchTileData();
	u8   BackgroundPixel();
	u8   SpritePixel(u8 &index);
	void RenderPixel();
	u32  FetchSpritePattern(int i, int row);
	void EvaluateSprites();
	void Tick(CPU *cpu);

private:
	const u32 colors[64]
	{
		0x666666, 0x002A88, 0x1412A7, 0x3B00A4, 0x5C007E, 0x6E0040, 0x6C0600, 0x561D00,
		0x333500, 0x0B4800, 0x005200, 0x004F08, 0x00404D, 0x000000, 0x000000, 0x000000,
		0xADADAD, 0x155FD9, 0x4240FF, 0x7527FE, 0xA01ACC, 0xB71E7B, 0xB53120, 0x994E00,
		0x6B6D00, 0x388700, 0x0C9300, 0x008F32, 0x007C8D, 0x000000, 0x000000, 0x000000,
		0xFFFEFF, 0x64B0FF, 0x9290FF, 0xC676FF, 0xF36AFF, 0xFE6ECC, 0xFE8170, 0xEA9E22,
		0xBCBE00, 0x88D800, 0x5CE430, 0x45E082, 0x48CDDE, 0x4F4F4F, 0x000000, 0x000000,
		0xFFFEFF, 0xC0DFFF, 0xD3D2FF, 0xE8C8FF, 0xFBC2FF, 0xFEC4EA, 0xFECCC5, 0xF7D8A5,
		0xE4E594, 0xCFEF96, 0xBDF4AB, 0xB3F3CC, 0xB5EBF2, 0xB8B8B8, 0x000000, 0x000000,
	};

	void SetPixel(int x, int y, u8 c);
};

#endif /* PPU_H */