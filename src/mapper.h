#ifndef MAPPER_H
#define MAPPER_H

#include "common.h"

class Mapper
{
public:
	~Mapper();
	virtual u8 Read(u16 addr) = 0;
	static Mapper *Load(const char *path);

protected:
	static u8 *PRG;
	static u8 *CHR;
	static size_t sizePRG;
	static size_t sizeCHR;
};

class Mapper0 : public Mapper
{
public:
	u8 Read(u16 addr);
};

#endif /* MAPPER_H */